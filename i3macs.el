;;; package --- i3macs
;;; Commentary:
;;; Code:
(require 'i3)
(require 'utf7)

(defun i3-choose-window ()
  (interactive)
  (let* ((windows (i3--get-windows (i3-get-tree-layout)))
		 (choice (completing-read "Focus: " windows )))
	(i3--focus-window (plist-get windows choice 'equal))))

(defun i3--focus-window (id)
  (i3-command 0 (concat "[id=\"" (number-to-string id) "\"] focus")))

(defun i3--window-name-id (node)
  (list (utf7-decode (cdr (assoc 'title (assoc 'window_properties node))))
		(cdr (assoc 'window node))))

(defun i3--window-name-ids (nodes)
  (cond
   ((seq-empty-p nodes) '())
   ((seq-rest (assoc 'window (seq-first nodes)))
	(append (i3--window-name-ids (seq-rest nodes))
			(i3--window-name-id (seq-first nodes))))
   (t (i3--window-name-ids (seq-rest nodes)))))

(defun i3--get-windows (tree)
  (cond
   ((seq-empty-p (assoc 'nodes tree)) '())
   (t (seq-reduce
	   (lambda (ws node) (append ws (i3--get-windows node)))
	   (seq-rest (assoc 'nodes tree))
	   (i3--window-name-ids (seq-rest (assoc 'nodes tree)))))))

(defun i3-choose-window-frame ()
  (let ((frame (make-frame '((auto-raise . t)
							 (minibuffer . only)
							 (width . 0.3)
							 (title . "emacs window focuser")
							 (height . 0.2)
							 (top . 0.5)
							 (left . 0.5))))
		(buff (get-buffer-create (generate-new-buffer-name "*window chooser*")))
		(org-mini-height max-mini-window-height))
	(select-frame frame)
	(select-frame-set-input-focus frame)
	(with-current-buffer buff
	  (unwind-protect
		  (progn (setq max-mini-window-height 1.0) ;; for some reason setq-local doesnt work
				 (i3-choose-window))
		(setq max-mini-window-height org-mini-height)
		(kill-buffer buff)
		(delete-frame frame)))))

(provide 'i3macs)

;;; i3macs.el ends here
