(setq build-list (list '("Debug" "make BUILD_MODE=DEBUG")
		       '("Debug RUN" "make BUILD_MODE=DEBUG && ./build/i3macs.o")
		       '("Release" "make BUILD_MODE=RELEASE")
		       '("Clean" "make clean")
			     ))

(provide '.build-config)
;;; .build-config ends here
