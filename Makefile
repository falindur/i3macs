BUILD_MODE = DEBUG

CC = gcc
SRCDIR = src/
BUILDDIR = build/
SRC = $(wildcard $(SRCDIR)*.c)
OBJ = $(SRC:.c=.o)

CC = gcc
IDIR = include/
LDIR = lib/
CFLAGS = -I$(IDIR) -L$(LDIR) -std=c11 `pkg-config --libs --cflags i3ipc-glib-1.0`
CLIBS = -ldl -lX11 -lxcb


ifeq ($(BUILD_MODE),DEBUG)
	CFLAGS += -Wall
endif

# $(BUILDDIR)%.o: $(SRCDIR)%.c $(DEPS)
# 	$(CC) -c $< -o $@ $(CFLAGS)

i3macs : $(OBJ)
	$(CC) $(OBJ) -o $(BUILDDIR)$@.o $(CFLAGS) $(LIBS) $(CLIBS) 

.PHONY : clean
clean :
	rm $(BUILDDIR)i3macs.o $(OBJ)
