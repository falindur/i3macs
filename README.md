# i3macs 
This project is mainly to improve my own workflow with the i3wm. 
Also to learn about X11, and "fuzzy" string matching.
A dmenu like menu that lists current windows and are selectable through
typing and C-n and C-p. After selecting with RET it focuses the window
selected.
Similar to the minibuffer with swith-to-buffer command in emacs.
using the i3 ipc to communicate with i3. Probably also some x11 stuff.
# todo
## C version
- [X] ability to getting the window titles and id lists
- [ ] drawing on screen
- [ ] selecting with keyboard 
- [ ] fuzzy matching with input
## Elisp version
- [X] ability to getting the window titles and id lists
- [X] drawing on screen
- [X] selecting with keyboard 
- [X] fuzzy matching with input
