#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <xcb/xcb.h> 
#include <i3ipc-glib/i3ipc-glib.h>

enum KEYCODE {
  KEYCODE_P = 33,
  KEYCODE_RET = 36,
  KEYCODE_G = 42,
  KEYCODE_N = 57
};

enum MODIFIERS {
  MOD_SHIFT = 1,
  MOD_CTRL = 4,
  MOD_ALT = 8
};

typedef struct i3_window {
  char name[128];
  guint id;
} i3_window;

typedef struct window_list {
  int length;
  i3_window* windows;
} window_list;

int min(int amount,...) {
  va_list numbers;
  va_start(numbers, amount);
  int min = INT32_MAX;
  int value = va_arg(numbers, int);
  for(int i = 0; i < amount; i++) {
	if (value < min) min = value;
	value = va_arg(numbers, int);
  }
  va_end(numbers);
  return min;
}

int lev(const char* first, const char* second, int sum) {
  if (first == NULL || second == NULL) return sum;
  if (!strcmp(first, "\0")) return sum + strlen(second);
  if (!strcmp(second, "\0")) return sum + strlen(first);
  if (first[0] == second[0]) return lev(++first, ++second, sum);
  return 1 + min(3,
				 lev(++first, second, sum), //always amount of numbers first
				 lev(first, ++second, sum),
				 lev(++first, ++second, sum));
}

//we use this to get the windows/order, but we could use i3ipc_con_find_named(). But making it yourself is more fun.
int lev_nr(const char* first, const char* second, int sum) {
  //non recursive version of levenshtein distance
  //this needs to be looked after, not really happy with this implementation
  int first_len = strlen(first);
  int second_len = strlen(second);
  int matrix[first_len+1][second_len+1];
  matrix[0][0] = 0;
  for(int i = 1; i <= first_len; i++) {	matrix[i][0] = i; }
  for(int j = 1; j <= second_len; j++) { matrix[0][j] = j; }
  for (int i = 1; i <= first_len; i++) {
	for (int j = 1; j <= second_len; j++) {
	  int score = 0;
	  if (first[i-1] != second[j-1]){
		score = 1;
	  }
	  matrix[i][j] = min(3, matrix[i-1][j] + 1,
						 matrix[i][j-1] + 1,
						 matrix[i-1][j-1] + score); 
	}
  }
  return matrix[first_len][second_len];
}

int matching_score(const char* first, const char* second) {
  return lev_nr(first, second, 0);
}


//this is maybe a function that is not needed if using the ipc library
window_list* get_windows(i3ipcConnection* conn){
  GList* descendents = i3ipc_con_leaves(i3ipc_connection_get_tree(conn, NULL));
  window_list* wlist = (window_list*)malloc(sizeof(window_list));
  wlist->length = g_list_length(descendents);
  wlist->windows = (i3_window*)malloc(sizeof(i3_window) * wlist->length);
  for(int i = 0; i < wlist->length; i++){
	i3ipcCon *con = I3IPC_CON(g_list_nth_data(descendents, i));
	strncpy(wlist->windows[i].name,  i3ipc_con_get_name(con), 120); 
	wlist->windows[i].id = i3ipc_con_get_id(con); //This uses our own ipc_con_get_id
	printf("name of con: %s, %d\n", wlist->windows[i].name,
		   wlist->windows[i].id);
  }
  g_list_free(descendents);
  return wlist;
}

void focus_i3_window(window_list* window_list, int focus_id,
					 i3ipcConnection* connection) {
  /* int id = 0; */
  /* for(int i = 1; i < argc; i++){ */
  /* 	float score = INT32_MAX; */
  /* 	for(int j = 0; j < window_list->length; j++){ */
  /* 	  float temp_score = (float)matching_score(argv[i], */
  /* 											   window_list->windows[j].name) / */
  /* 		(float)strlen(window_list->windows[j].name); */
  /* 	  if (temp_score < score) { */
  /* 		score = temp_score; */
  /* 		id = j; */
  /* 	  } */
  /* 	} */
  /* } */
  char command[64];
  printf("window name: %s\n", window_list->windows[focus_id].name);
  sprintf(command, "[id=\"%lu\"] focus", window_list->windows[focus_id].id);
  printf(command);
  char *reply = i3ipc_connection_message(connection, I3IPC_MESSAGE_TYPE_COMMAND,
										 command, NULL);
  g_free(reply);
}

void open_x_window(window_list* window_list,
				   i3ipcConnection* i3connection) {
  /* Open the connection to the X server */
  xcb_connection_t *connection = xcb_connect (NULL, NULL);

  /* Get the first screen */
  xcb_screen_t *screen = xcb_setup_roots_iterator (xcb_get_setup (connection)).data;
  
  /* Create black (foreground) graphic context */
  xcb_drawable_t  window     = screen->root;
  xcb_gcontext_t  foreground = xcb_generate_id (connection);
  uint32_t        mask       = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;
  uint32_t        values[2]  = {screen->black_pixel, 0}; //what do these values mean?

  xcb_create_gc (connection, foreground, window, mask, values);
  /* Create a window */
  window = xcb_generate_id (connection);
  mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK; //how to know these masks??
  values[0] = screen->white_pixel;
  values[1] = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE;
  xcb_create_window (connection,                    /* connection          */
					 XCB_COPY_FROM_PARENT,          /* depth               */
					 window,                        /* window Id           */
					 screen->root,                  /* parent window       */
					 0, 0,                          /* x, y                */
					 150, 150,                      /* width, height       */
					 1,                            /* border_width        */
					 XCB_WINDOW_CLASS_INPUT_OUTPUT, /* class               */
					 screen->root_visual,           /* visual              */
					 mask, values);                /* masks */
  xcb_font_t font = xcb_generate_id(connection);
  xcb_open_font(connection, font,
				strlen("lucidasanstypewriter-bold-8"),
				"lucidasanstypewriter-bold-8");
  xcb_gcontext_t  font_gc    = xcb_generate_id (connection);
  mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND | XCB_GC_FONT;
  uint32_t values_list[3]  = {screen->black_pixel, screen->white_pixel, font}; //what do these values mean?

  xcb_create_gc (connection, font_gc, window, mask, values_list);
  /* Map the window on the screen and flush*/
  /* const static uint32_t values_list_3[] = { XCB_STACK_MODE_ABOVE }; */
  /* xcb_configure_window (connection, window, XCB_CONFIG_WINDOW_STACK_MODE, values_list_3); */

    /* Move the window on the top of the stack */
  xcb_map_window (connection, window);
  xcb_flush (connection);

  int highlight_window_id = 0;
  /* draw primitives */
  xcb_generic_event_t *event;
  xcb_rectangle_t rectangle[] = {{45, 10, 400, 10}};
  while ((event = xcb_wait_for_event (connection))) {
	switch (event->response_type & ~0x80) {
	case XCB_EXPOSE:
	  printf("INFO: expose!!!\n");
	  for (int i = 0; i < window_list->length; i++) {
		if (i == highlight_window_id) {
		  printf("INFO: %d and %highlight_window_id\n", i, highlight_window_id);
		  rectangle[0].y = 40 + (i * 10);
		  xcb_poly_rectangle (connection, window, foreground, 1, rectangle);
		}
		xcb_image_text_8(connection,
						 strlen(window_list->windows[i].name),
						 window,
						 font_gc,
						 50, 50 + (i * 10),
						 window_list->windows[i].name);
	  }
	  /* flush the request */
	  xcb_flush (connection);
	  break;
	case XCB_KEY_RELEASE:
	  xcb_key_release_event_t *kr = (xcb_key_release_event_t *)event;
	  printf("state of key released: %d\n", kr->state);
	  printf("key released: %d\n", kr->detail);
	  
	  printf ("Key released in window %d\n",
			  kr->event);
	  if (kr->detail == KEYCODE_N && kr->state == MOD_CTRL){
		highlight_window_id += 1;
		xcb_send_event(connection, 0, window, XCB_EVENT_MASK_EXPOSURE, "hoi");
		xcb_flush(connection);
	  }
	  if (kr->detail == KEYCODE_P && kr->state == MOD_CTRL){
		highlight_window_id -= 1;
		/* xcb_clear_area(connection, 2, window, window->x, window->y, window->width, window->height); */
		/* xcb_expose_event_t *e = (xcb_expose_event_t*)malloc(sizeof(xcb_expose_event_t)); */
		/* memset(e, 0, sizeof(xcb_expose_event_t)); */
		/* e->response_type = XCB_EXPOSE; */
		/* e->window = window; */
		/* e->x = 50; */
		/* e->y = 50; */
		/* e->width = 50; */
		/* e->height = 50; */
		/* e->count = 0; */
		/* xcb_send_event(connection, 0, window, XCB_EVENT_MASK_EXPOSURE, (char*)e); */
		xcb_flush(connection);
	  }
	  if (kr->detail == KEYCODE_RET){
		focus_i3_window(window_list, highlight_window_id, i3connection);
		free(event);
		xcb_disconnect(connection);
		return ;
	  }
	  break;
	case XCB_KEY_PRESS:
	  xcb_key_release_event_t *kp = (xcb_key_release_event_t *)event;
	  printf("state of key pressed: %d\n", kp->state);
	  printf("key pressed: %d\n", kp->detail);
	  
	  printf ("Key pressed in window %d\n",
			  kp->event);
	  if (kp->detail == KEYCODE_G && kp->state == MOD_CTRL){
		free(event);
		xcb_disconnect(connection);
		return ;
	  }
	  break;  
	default: 
	  /* Unknown event type, ignore it */
	  printf("INFO: event happend");
	  break;
	}
	free (event);
  }
  xcb_close_font(connection, font);
  xcb_disconnect(connection);
}

int main(int argc, char *argv[]){
  i3ipcConnection *conn = i3ipc_connection_new(NULL, NULL);
  window_list* window_list = get_windows(conn);
  open_x_window(window_list, conn);
  g_object_unref(conn);
  return 0;
}
